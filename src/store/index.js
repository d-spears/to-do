import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    tasks: [],
    nextTaskId: 0,
  },
  mutations: {
    addTask(state, payload) {
      state.tasks.push({
        text: payload.text,
        done: false,
        id: state.nextTaskId,
      });
      state.nextTaskId++;
    },
    deleteTask(state, payload) {
      const index = state.tasks.indexOf(
        state.tasks.find((x) => x.id === payload.id)
      );
      if (index > -1) {
        state.tasks.splice(index, 1);
      }
    },
    updateTask(state, payload) {
      const index = state.tasks.indexOf(
        state.tasks.find((x) => x.id === payload.id)
      );
      if (index > -1) {
        state.tasks[index].done = payload.status;
        localStorage.setItem("tasks", JSON.stringify(state.tasks));
      }
    },
    restoreTaskId(state) {
      state.nextTaskId = localStorage.nextTaskId;
    },
    restoreTasks(state) {
      state.tasks = JSON.parse(localStorage.getItem("tasks"));
    },
    reset(state, payload) {
      state.tasks = [];
      state.nextTaskId = 0;
      payload.d.value = false;
    },
  },
  getters: {
    tasksStarted(state) {
      return state.tasks.length;
    },
    progress(state) {
      return (
        (state.tasks.filter((task) => task.done).length / state.tasks.length) *
        100
      );
    },
  },
});

export default store;
